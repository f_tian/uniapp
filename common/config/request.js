// #ifndef H5
	var url = '服务器接口地址'
// #endif
  
// #ifdef H5
	if(process.env.NODE_ENV === 'development') {
		var url = '127.0.0.1'
	}else{
		// var url = window.location.host
		var url = '服务器接口地址'
	}
// #endif

var socketurl = '1.14.151.79'

export let apiUri = 'http://' + url + '/api/'

export let imgUri = 'http://' + url + '/'

export let socketUri = 'ws://' + socketurl + ':8181'

export let HTTP_REQUEST_URL = 'http://' + url

export let SPREAD_URL = 'http://' + url + '/index/#/pages/users/login/index'

// 回话密钥名称 请勿修改此配置
export let TOKENNAME = 'Authorization'

// 缓存时间 0 永久
export let EXPIRE = 0

export let HEADER = { 'content-type': 'application/json' }