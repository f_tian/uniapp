import request from "@/common/library/request.js";

// 生成订单
export function createOrder(data){ return request.post('User/set_order', data) }

// 微信支付(小程序)
export function wxPay(id){ return request.post('User/wachat_xiao_pay', { order_id: id }) }

// 微信支付(H5)
export function wxPayH5(id){ return request.post('User/wachat_geng_pay', { order_id: id }) }

// 余额支付
export function payment(id){ return request.post('User/placeOrderYue', { order_id: id }) }

// 订单列表  @param object data
export function orderList(data){ return request.get('User/getUserOrderList', data) }

// 订单详情 @param string id
export function orderDetail(id){ return request.get('User/getUserOrderOne',{ id: id }) }

// 获取核销二维码
export function payQrCode(id) { return request.get("order/payQryCode?order_id=" + id) }

// 订单确认获取订单详细信息 @param string cartId
export function orderConfirm(cartId){ return request.post('order/confirm', { cartId: cartId }) }

// 计算订单金额 @param key @param data @returns {*}
export function orderComputed(key, data) { return request.post("order/computed/" + key, data) }

// 订单统计数据
export function orderData(){ return request.get('User/getOrderCount') }