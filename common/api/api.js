import request from "@/common/library/request.js"

// 验证码发送(注册)
export function registerVerify(data) { return request.get("register/code", data, { noAuth : true }) }

// 验证码发送(修改密码)
export function retrieveVerify(data) { return request.get("retrieve/code", data, { noAuth : true }) }

// 注册
export function register(data) { return request.post("register", data, { noAuth : true }) }

// 用户登录
export function loginPassword(data) { return request.post("login", data, { noAuth : true }) }

// 用户手机号修改密码 @param data object 用户手机号 验证码 密码
export function resetPassword(data) { return request.post("retrieve", data, { noAuth: true }) }

// 获取登录logo
export function logo() { return request.get('wechat/get_logo', {}, { noAuth : true}) }

// 首页产品的轮播图和产品信息 @param int type
export function groomList(type, data){ return request.get('groom/list/' + type, data, { noAuth: true }) }

// 获取分类列表
export function categoryList(){ return request.post('User/store_category', {}, { noAuth:true}) }

// 获取推荐产品
export function productHot(page, limit) {
  return request.get("product/hot", { page: page === undefined ? 1 : page, limit: limit === undefined ? 4 : limit }, { noAuth: true })
}

// 获取主页数据 无需授权
export function getIndexData() { return request.get("index", {}, { noAuth : true}) }

// 获取产品列表 @param object data
export function productslist(data = {}){ return request.post('User/StoreProductList', data, { noAuth: true }) }

// 获取产品详情 @param int id
export function productDetail(id){ return request.get('User/StoreProductOne', { id: id }, { noAuth : true }) }

// 获取首页轮播图
export function indexBanner(){ return request.post('Index/banner', {}, { noAuth : true }) }

// 微信登录
export function wxLogin(data){ return request.post('User/code_login', data, { noAuth : true }) }

// 首页商品
export function storeProductList(data = {}){ return request.post('User/StoreProductList', data, { noAuth : true }) }

// 微信公众号分享配置信息
export function wxShare(url){ return request.post('User/getSignPackage', { url: url }, { noAuth : true }) }