import request from "@/common/library/request.js"

// 获取绑定银行卡列表
export function bankList(data = {}){ return request.get('bank/list', data) }

// 添加绑定银行卡
export function addBank(data = {}){ return request.post('bank/increase', data) }

// 添加绑定银行卡
export function deleteBank(data = {}){ return request.post('bank/delete', data) }

// 获取收款二维码
export function collectionQRCode(data = {}){ return request.get('collection/QRCode', data) }