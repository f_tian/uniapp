import request from "@/common/library/request.js"

// 获取用户信息
export function videoList(data = {}){ return request.post('User/video_list', data) }

// 视频点赞/取消
export function praise(data = {}){ return request.post('User/dianzan', data) }