import request from "@/common/library/request.js"

// 获取用户信息
export function userInfo(){ return request.get('user/info') }

// 修改个人信息
export function userEdit(data){ return request.post('user/update', data) }

// 地址列表
export function addressList(data){ return request.get('Address/getAddressList', data) }

// 获取单个地址
export function addressDetail(id = ''){ return request.get('Address/getAddressOne', { id: id }) }

// 修改 添加地址 @param object data
export function editAddress(data){ return request.post('Address/addEditAddress', data) }

// 删除地址
export function delAddress(id){ return request.post('Address/delAddress', { id: id }) }

// 获取分销海报图片
export function spreadBanner(data){ return request.get('User/banner', data) }

// 修改密码 @param pwd 旧密码 nowpwd 新密码
export function setPwd(data){ return request.post('User/setUserPwd', data) }

// 获取推广海报
export function poster(url){ return request.get('user/poster', { url: url }) }

// 获取流水
export function moneyLog(data = {}){ return request.post('User/getMoneyLog', data) }

// 我的团队
export function myTeam(data = {}){ return request.post('User/getSubordinate', data) }

// 收益排行
export function profitRank(data = {}){ return request.post('User/userList', data) }

// 团队排行
export function teamRank(data = {}){ return request.post('User/myUserList', data) }

// 收藏&浏览记录列表
export function careAndBrowse(data = {}){ return request.post('User/collection_log_list', data) }

// 删除收藏&浏览记录
export function cleanRecord(data = {}){ return request.post('User/delcollection_log', data) }

// 删除收藏&浏览记录
export function care(id){ return request.post('User/adddel_collection_log', { id: id }) }