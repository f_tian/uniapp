import { HTTP_REQUEST_URL, HEADER, TOKENNAME } from '@/common/config/request.js'
import store from '@/store/index.js'
import { wxLogin } from '@/common/api/api.js'

// 发送请求
function baseRequest(url, method, data = {}, {noAuth = false, noVerify = false}) {
  let Url = HTTP_REQUEST_URL, header = HEADER
	
  // 登录过期自动登录
	// #ifndef MP-WEIXIN
  if (!noAuth && !uni.getStorageSync('token')) {
  	uni.navigateTo({ url:'/pages/users/login/index' })
  	return Promise.reject({ msg:'未登陆' })
  }
	// #endif

  if (uni.getStorageSync('token')) header[TOKENNAME] = 'Bearer ' + uni.getStorageSync('token')
	
	if(process.env.NODE_ENV === 'development'){ console.log(Url + '/api/' + url) }
	
	// 异步调用
  return new Promise((reslove, reject) => {
    uni.request({
      url: Url + '/api/' + url,
      method: method || 'GET',
      header: header,
			sslVerify: false,
      data,
      success: (res) => {
        if (noVerify) {
					reslove(res.data, res)
				
					if(process.env.NODE_ENV === 'development'){console.log(res.data)}
					
				} else if (res.data.status == 200){
					reslove(res.data, res)
					
					if(process.env.NODE_ENV === 'development'){console.log(res.data)}
					
				} else if ([403, 401, 402].indexOf(res.data.status) !== -1) {
					
					// #ifdef MP-WEIXIN
					uni.login({
						provider: 'weixin',
						success: res => {
							wxLogin({ code: res.code, spread_uid: store.state.spreadID }).then(res => {
								uni.setStorageSync('token', res.data.token)
								
								if(process.env.NODE_ENV === 'development'){console.log('Got the token')}
								
								baseRequest(url, method, data, { noAuth, noVerify }).then(r => {
									reslove(r)
								})
							})
						}
					})
					// #endif
					
					// #ifndef MP-WEIXIN
					uni.reLaunch({ url: '/pages/users/login/index' })
					reject(res.data)
					// #endif
					
        } else {
					if(process.env.NODE_ENV === 'development'){ console.log("返回错误: " + JSON.stringify(res.data)) }
					
					reject(res.data.msg || '系统错误')
			 }
      },
      fail: (msg) => {
				if(process.env.NODE_ENV === 'development'){ console.log(JSON.stringify(msg)) }
				reject('请求失败')
			}
    })
  })
}

const request = {};

['options', 'get', 'post', 'put', 'head', 'delete', 'trace', 'connect'].forEach((method) => {
  request[method] = (api, data, opt) => baseRequest(api, method, data, opt || {})
})

export default request