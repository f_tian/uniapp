import { TOKENNAME, HTTP_REQUEST_URL } from '@/common/config/request.js'
export default {
 	// 单图上传 @param object opt, successCallback 成功执行方法, errorCallback 失败执行方法
 	uploadImageOne: function(opt, successCallback, errorCallback) {
 		if (typeof opt === 'string') { 
			let url = opt; opt = {}; opt.url = url // 如果传参为字符串则只配置url, 其他默认
		}
 		let count = opt.count || 1, // 默认单选
				sizeType = opt.sizeType || ['compressed'], // 文件压缩类型(original 原图, compressed 压缩图)
				sourceType = opt.sourceType || ['album', 'camera'], // 指定图片来源(相册和相机)
				uploadUrl = opt.url || '', // 上传服务器uri
				inputName = opt.name || 'img', // formData文件key(与后端约定)
				sort = opt.sort || 'avatar'; // 图片类别(便于后端分类存储)
 		uni.chooseImage({
 			count: count, // 最多可以选择的图片总数  
 			sizeType: sizeType, 	// 可以指定是原图还是压缩图，默认二者都有  
 			sourceType: sourceType, 	// 可以指定来源是相册还是相机，默认二者都有  
 			success: function(res) {
 				uni.showLoading({ title: '图片上传中' });	// 启动上传等待中... 
				uni.uploadFile({
					url: HTTP_REQUEST_URL + '/api/' + uploadUrl,
					filePath: res.tempFilePaths[0],
					name: inputName,
					formData: { 'sort': sort },
					header: {
						// #ifdef MP
						"Content-Type": "multipart/form-data",
						// #endif
						[TOKENNAME]: 'Bearer ' + uni.getStorageSync('token')
					},
					success: function(res) {
						uni.hideLoading()
						if (res.statusCode == 403) { uni.showToast({ title: res.data, icon: 'loading' }) } 
						else {
							let data = res.data ? JSON.parse(res.data) : {}
							if (data.status == 200) { successCallback && successCallback(data) } 
							else { errorCallback && errorCallback(data); uni.showToast({ title: data.msg, icon: 'loading' }) }
						}
					},
					fail: function(res) { uni.hideLoading(); uni.showToast({ title: '上传图片失败', icon: 'loading' }) }
				})
 			}
 		})
 	},
	
	// 生成海报获取文字 @param text 为传入的文本, num 为单行显示的字节长度; @return array
	textByteLength: function(text, num) {
		let strLength = 0;
		let rows = 1;
		let str = 0;
		let arr = [];
		for (let j = 0; j < text.length; j++) {
			if (text.charCodeAt(j) > 255) {
				strLength += 2;
				if (strLength > rows * num) {
					strLength++;
					arr.push(text.slice(str, j));
					str = j;
					rows++;
				}
			} else {
				strLength++;
				if (strLength > rows * num) {
					arr.push(text.slice(str, j));
					str = j;
					rows++;
				}
			}
		}
		arr.push(text.slice(str, text.length));
		return [strLength, arr, rows] //  [处理文字的总字节长度，每行显示内容的数组，行数]
	},
	
	// 获取分享海报 @param arr2 海报素材, store_name 素材文字, price 价格, successFn 回调函数
	PosterCanvas: function(arr2, store_name, price, successFn) {
		uni.showLoading({ title: '海报生成中', mask: true })
		const ctx = uni.createCanvasContext('myCanvas')
		ctx.clearRect(0, 0, 0, 0)
		// 只能获取合法域名下的图片信息,本地调试无法获取
		uni.getImageInfo({
			src: arr2[0],
			success: (res) => {
				const WIDTH = res.width;
				const HEIGHT = res.height;
				ctx.drawImage(arr2[0], 0, 0, WIDTH, HEIGHT);
				ctx.drawImage(arr2[1], 0, 0, WIDTH, WIDTH);
				ctx.save();
				let r = 90;
				let d = r * 2;
				let cx = 40;
				let cy = 990;
				ctx.arc(cx + r, cy + r, r, 0, 2 * Math.PI);
				// ctx.clip();
				ctx.drawImage(arr2[2], cx, cy,d,d);
				ctx.restore();
				const CONTENT_ROW_LENGTH = 40;
				let [contentLeng, contentArray, contentRows] = this.textByteLength(store_name, CONTENT_ROW_LENGTH);
				if (contentRows > 2) {
					contentRows = 2;
					let textArray = contentArray.slice(0, 2);
					textArray[textArray.length - 1] += '……';
					contentArray = textArray;
				}
				ctx.setTextAlign('center');
				ctx.setFontSize(32);
				let contentHh = 32 * 1.3;
				for (let m = 0; m < contentArray.length; m++) ctx.fillText(contentArray[m], WIDTH / 2, 820 + contentHh * m)
				ctx.setTextAlign('center')
				ctx.setFontSize(48);
				ctx.setFillStyle('red')
				ctx.fillText('￥' + price, WIDTH / 2, 880 + contentHh)
				ctx.draw(false, setTimeout(()=>{
				    uni.hideLoading(); successFn && successFn(WIDTH, HEIGHT)
				},500))
				
				// ctx.draw(true, (function() {
				// 	uni.canvasToTempFilePath({
				// 		canvasId: 'myCanvas',
				// 		destWidth: WIDTH,
				// 		destHeight: HEIGHT,
				// 		success: function(res) {
				// 			console.log(res)
				// 			// uni.hideLoading(); successFn && successFn(res.tempFilePath) 
				// 		},
				// 		fail: function(err) { console.log(e) }
				// 	})
				// })())
			},
			fail: function(err) { uni.hideLoading(); uni.showToast({ title: '无法获取图片信息', icon: 'loading' }) }
		})
	},
}
