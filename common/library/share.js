var wx = require("weixin-js-sdk/index.js") // 引入刚下载的js文件
import { wxShare } from '@/common/api/api.js' // 请求分享配置后端接口

export default {
	// 初始化
	initJssdkShare: function(callback, url) {
		var url = url
		console.log(url);
		// 这一步需要调用后台接口,返回需要的签名 签名时间戳 随机串 和公众号appid
		// 注意url:window.location.href.split('#')[0] //
		wxShare(url).then(res => {
			console.log(res)
				wx.config({
					// debug: true,	// 调试的时候需要 在app上回弹出errmg:config ok 的时候就证明没问题了 这时候就可以改为false
					appId: res.appId,	// appid
					timestamp: res.timestamp,	// 时间戳
					nonceStr: res.nonceStr,	// 随机串 
					signature: res.signature,	// 签名
					jsApiList: [	// 必填 是下面需要用到的方法集合
						"updateAppMessageShareData",
						"updateTimelineShareData",
						"onMenuShareTimeline",
						"onMenuShareAppMessage",
						"onMenuShareQQ"
					]
				})
				if(callback){
					callback()
				}
		})
	},
	// data为传参, url是当前页面的链接
	share:function(data, url){
		this.initJssdkShare(function(){
			wx.ready(function(){
				console.log("llllllllllll");
				console.log(data);
				var sharedata={
					title: data.title,	// 标题
					desc: data.desc, 		// 描述
					link: data.link ,		// 分享链接
					imgUrl:data.imgUrl, // 图片
					success:(res=>{
					})
				};
				wx.updateAppMessageShareData(sharedata)	// 自定义微信分享给朋友
				wx.updateTimelineShareData(sharedata)		// 自定义微信分享给朋友
				wx.onMenuShareAppMessage(sharedata)			// 获取“分享给朋友”按钮点击状态及自定义分享内容接口（即将废弃）
			})
		}, url)
	}
}

