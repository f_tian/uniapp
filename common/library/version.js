import * as utils from '@/uni_modules/hic-upgrade/utils/index.js' // 引用组件使用工具
import request from "@/common/library/request.js" // 引入网络接口调用封装方法

/**
 * @description 版本更新
 * @param {Boolean} tip 是否显示提示信息
 *  app初始化进行版本判断, 不显示提示信息   -> false
 *  关于页面 手动点击版本更新, 显示提示信息   -> true
 */
export async function versionUpdate (tip) {
	// 1 请求返回服务端版本信息
	const result = await getLatestVersion({
		platform: utils.platform // 运行平台
	});
	const res = result.data
	if (!res) {
		// if (tip) uni.$z.toast('暂无更新')
		if (tip) uni.showToast({ title: '暂无更新', icon: 'none' })
		return
	}
	// 2 存储服务端版本信息
	utils.setServerInfo({
			version: res.versionName, // 版本名称
			versionCode: res.versionCode, // 版本号(判定更新依据)
			downloadUrl: res.downloadUrl, // 下载地址
			wgtUrl: res.wgtUrl, // 热更包地址
			forceUpdate: res.forceUpdate, // 是否强制更新
			isSilently: res.isSilently, // 是否静默更新
			desc: res.versionDesc // 版本更新说明
	});
	// 3 判断是否更新
	if (!utils.isUpdate()) {
		// if (tip) uni.$z.toast('已是最新版本')
	if (tip) uni.showToast({ title: '已是最新版本', icon: 'none' })
		return;
	}
uni.hideLoading()
	const updateInfo = utils.getUpdateInfo();
	const info = utils.getServerVersion();
	// 4 wgt 静默更新
	if (updateInfo.type == 'wgt' && info.isSilently) {
		const path = await utils.downloadFile(updateInfo.url);
		// 下载完成 直接安装 下次启动生效
		await utils.installForAndroid(path);
		return;
	}
	// 5 跳转页面 提示更新
	await utils.toUpgradePage('/uni_modules/hic-upgrade/pages/upgrade');
}

/**
 * 真实服务端请求
 */
function getLatestVersion(data = {}) { `这里配置获取服务器请求最新版本号接口`
	return request.get("upgrade", data, { noAuth : true })
}