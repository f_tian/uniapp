export function formatPhone(phone) {
	return phone.substring(0, 3) + '****' + phone.substr(phone.length - 4)
}

export function formatBankNumber(number) {
	if (typeof number == 'number') number = number.toString()
	var sum = ' **** **** '
	// for (let i = 0; i < number.length - 8; i++) {
	// 	sum += '*'
	// }
	return number.substr(0,4) + sum + number.substr(-4)
}

export function formatName(txt) {
	var newStr;
	if (txt.length === 2) {
			newStr = txt.substr(0, 1) + '*';
	} else if (txt.length > 2) {
			var char = '';
			for (let i = 0, len = txt.length - 2; i < len; i++) { char += '*' }
			newStr = txt.substr(0, 1) + char + txt.substr(-1, 1);
	} else { newStr = txt }
	return newStr
}

export function formatDate(time) {
	if (!time) {
		return ' '
	} else {
		const timeLen = time.length
		const oneDate = new Date(parseInt(time) * 1000)
		const twoDate = new Date(parseInt(time))
		const date = timeLen == 10 ? oneDate : twoDate
		const dateNumFun = (num) => num < 10 ? `0${num}` : num 
		const [Y, M, D, h, m, s] = [
			date.getFullYear(),
			dateNumFun(date.getMonth() + 1),
			dateNumFun(date.getDate()),
			dateNumFun(date.getHours()),
			dateNumFun(date.getMinutes()),
			dateNumFun(date.getSeconds())
		]
		return `${Y}-${M}-${D} ${h}:${m}:${s}`
	}
}