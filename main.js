import App from './App'
import store from './store'
import messages from './locale/index'
import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.prototype.$browser = function() {
	return uni.getStorageSync('browser')
}

let i18nConfig = {
  locale: uni.getLocale(),
  messages
}

Vue.use(VueI18n)
const i18n = new VueI18n(i18nConfig)
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
  i18n,
  ...App
})
app.$mount()