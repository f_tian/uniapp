import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
  state:{ // 存放状态
    "userInfo": null,
		"spreadID": null
  },
  getters: {
    isLogin: state => { // 获取登录状态
      return !!uni.getStorageSync('token')
    }
  },
  mutations: {
  	LOGIN(state, opt) { // 登录后缓存token
  		uni.setStorageSync('token', opt.token)
      uni.setStorageSync('time', opt.time)
  	},
    LOGOUT(state) { // 退出后清除缓存
    	uni.clearStorageSync()
      state.userInfo = null
    },
    UPDATE_USERINFO(state, userInfo) {
    	state.userInfo = userInfo
    },
		UPDATE_SPREAD(state, id) {
			state.spreadID = id
		}
  },
  actions: {
  	USERINFO({ state, commit }, userInfo) {
      return new Promise(reslove => {
        commit("UPDATE_USERINFO", userInfo)
        reslove(userInfo)
      })
    }
  }
})

export default store